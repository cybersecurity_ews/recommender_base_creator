'''
Created on Apr 25, 2016

@author: rodrigo
'''

import csv
import numpy as np
from scipy.stats import bernoulli
from Crypto.Random.random import randint
import random



def main():
    TOPN = 10
    N_USERS = 100
    N_ALERTS = 500

    list_provider = []
    list_product = []
    
    list_alerts = []
    
    with open('../dataset/data_cve_2015.csv', 'r') as csvfile:
        reader = csv.reader(csvfile)
        print reader.next()

        for row in reader:
            try:
                list_alerts.append(row)
                list_provider.append(row[2])
                list_product.append(row[3])
            except:
                print row
                
    freq_provider = calculate_frequency(list_provider)
    freq_product = calculate_frequency(list_product)
    
    #print sorted(freq_provider, key=freq_provider.__getitem__, reverse=True)
    #print sorted(list(freq_provider.values()), reverse=True)

    #print sorted(freq_product, key=freq_product.__getitem__, reverse=True)
    #print sorted(list(freq_product.values()), reverse=True)
    
    topn_providers = sorted(freq_provider, key=freq_provider.__getitem__, reverse=True)[:TOPN]
    topn_products = sorted(freq_product, key=freq_product.__getitem__, reverse=True)[:TOPN]

    n_providers = np.array(sorted(list(freq_provider.values()), reverse=True)[:TOPN])
    n_products = np.array(sorted(list(freq_product.values()), reverse=True)[:TOPN])
    
    perc_providers = n_providers/float(n_providers.sum())
    perc_products = n_products/float(n_products.sum())

    print "Sample of alerts:"
    print list_alerts[:3]
    
    print perc_products
    print perc_providers

    print "Providers: ", topn_providers
    print "Products: ", topn_products
    
    users_profiles = generate_profiles_users(N_USERS, topn_providers, topn_products, perc_providers, perc_products)
    print '\n'
    print users_profiles
    generate_ratings_users(users_profiles, list_alerts[:N_ALERTS], 100, 200)    
#    from scipy.stats import bernoulli
#    bernoulli.rvs(p=0.9, size=10)


#elements = np.array([1.1, 2.2, 3.3])
#probabilites = np.array([0.2, 0.5, 0.3])
#np.random.choice(elements, 10, p = list(probabilities))

def generate_profiles_users(n_users, providers, products, prob_providers, prob_products):
    # semente padrao
    random.seed(1)
    
    MINLIKE = 1
    MAXLIKE = 5
    
    users = []
    
    for i in range(n_users):
        n_providers = random.randint(MINLIKE, MAXLIKE)
        n_products = random.randint(MINLIKE, MAXLIKE)
        
        list_pref_providers = np.random.choice(providers, n_providers, p=prob_providers)
        list_pref_products = np.random.choice(products, n_products, p=prob_products)
        user_preferences = set(list(list_pref_providers) + list(list_pref_products))
        
        users.append(user_preferences)
    
    return users
    
def generate_ratings_users(users_profiles, alerts, min_rates_user, max_rates_user):
    print "ratings"
    #    bernoulli.rvs(p=0.9, size=10)
    #user_profile = users_profiles[0] #for testing (selecionar depois baseado no min e max rating.
    
    # arquivo de usuarios
    csvfile_users = open('../dataset/users.csv', 'w')
    users_writer = csv.writer(csvfile_users, quoting=csv.QUOTE_MINIMAL)
    header = ["user_id", "preferences"]
    users_writer.writerow(header)
    
    # arquivo de ratings
    csvfile_ratings = open('../dataset/ratings.csv', 'w')
    writer = csv.writer(csvfile_ratings, quoting=csv.QUOTE_MINIMAL)
    header = ["userid","cveid", "provider", "product", "cvss_score", "like", "dislike", "critical"]
    writer.writerow(header)
    
    # controla o id dos usuarios
    user_id = 1

    for user_profile in users_profiles:
        user_line = [user_id, list(user_profile)]
        users_writer.writerow(user_line)
        
        # numero de ratings (selecao de cve que poderao ser avaliados)
        n_ratings = randint(min_rates_user, max_rates_user)
        
        alerts_sample = random.sample(alerts, n_ratings)

        for alert in alerts_sample:
            provider = alert[2]
            product = alert[3]
            cvss_score = float(alert[1])
            cve_id = alert[0]
            
            user_preference = False
            like = dislike = critical = 0
            
            if provider in user_profile or product in user_profile:
                user_preference = True
            
            like = like_rating(cvss_score, user_preference)
            if like:
                critical = critical_rating(cvss_score, user_preference)
            else:
                dislike = dislike_rating(cvss_score, user_preference)
            
            if like or dislike or critical:
                rating_line = [user_id, cve_id, provider, product, cvss_score, like, dislike, critical]
                writer.writerow(rating_line)
            
        user_id += 1
    
    csvfile_ratings.close()
    csvfile_users.close()
            
              
#like rating based in user preferences
def like_rating(cvss_score, user_preference=True):
    if user_preference:
        if cvss_score == 10.0:
            return 1
        if cvss_score >= 9.0:
            return bernoulli.rvs(p=0.95)
        elif cvss_score >= 8.0:
            return bernoulli.rvs(p=0.8)
        elif cvss_score >= 7.0:
            return bernoulli.rvs(p=0.65)
        elif cvss_score >= 6.0:
            return bernoulli.rvs(p=0.5)
        elif cvss_score >= 5.0:
            return bernoulli.rvs(p=0.35)
        
        return bernoulli.rvs(p=0.1)
    else:
        if cvss_score >= 9.0:
            return bernoulli.rvs(p=0.75)
        elif cvss_score >= 8.0:
            return bernoulli.rvs(p=0.6)
        elif cvss_score >= 7.0:
            return bernoulli.rvs(p=0.45)
        
        return bernoulli.rvs(p=0.1)

#dislike rating based in user preferences        
def dislike_rating(cvss_score, user_preference=True):
    if user_preference:
        if cvss_score < 1.0:
            return bernoulli.rvs(p=0.5)
        elif cvss_score < 2.0:
            return bernoulli.rvs(p=0.4)
        elif cvss_score < 3.0:
            return bernoulli.rvs(p=0.3)
        elif cvss_score < 4.0:
            return bernoulli.rvs(p=0.2)
        elif cvss_score < 5.0:
            return bernoulli.rvs(p=0.1)
        
        return 0
    else:
        if cvss_score < 4.0:
            return bernoulli.rvs(p=0.7)
        elif cvss_score < 5.0:
            return bernoulli.rvs(p=0.5)
        elif cvss_score < 7.0:
            return bernoulli.rvs(p=0.4)
        
        return bernoulli.rvs(p=0.1)
    
def critical_rating(cvss_score, user_preference=True):
    if user_preference:
        if cvss_score >= 9.0:
            return bernoulli.rvs(p=0.9)
        elif cvss_score >= 7.0:
            return bernoulli.rvs(p=0.5)
        return 0  #not marked as critical
    else:
        if cvss_score >= 9.0:
            return bernoulli.rvs(p=0.8)
        elif cvss_score >= 7.0:
            return bernoulli.rvs(p=0.3)
        
        return 0

def calculate_frequency(list_values):
    """
    Calculate absolute frequency of elements in a list
    @param list_values: list of elements 
    """
    frequency = {}
    for value in list_values:
        if value in frequency:
            frequency[value] += 1
        else:
            frequency[value] = 1
        
    return frequency

if __name__ == '__main__':
    main()
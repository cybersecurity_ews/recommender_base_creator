'''
Created on Apr 23, 2016

Create a new dataset using vFeed that contains:
cveid, cvss_score, provider, product, date_published, date_modified, summary 

@author: rodrigo
'''

import json
from lib.core.methods import *
from database_controller import CveDatabase
import csv

def main():
    # get list of cveid from 2015
    cvedb = CveDatabase()
    cve_list = cvedb.search_cve('CVE-2015')  # busca os registros de 2015.
    
    print "Registros lidos: ", len(cve_list)
    
    csvfile = open('../dataset/data_cve.csv', 'w')
    writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL)
   
    header = ["cveid",  "cvss_score", "provider", "product", "date_published", "date_modified", "description"]
    writer.writerow(header)
    
    # get info about each cveid 
    for cve_row in cve_list:
        try:
            #print cve_row
            cveid = cve_row[0]
            cvss_score = cve_row[4]
            date_pub = cve_row[1]
            date_mod = cve_row[2]
            description = cve_row[3]
            
            cpe_list = cvedb.search_cpe(cveid)
            #print cpe_list
            fields = cpe_list[0][0].split(":")
            #print(fields)
            provider = fields[2]
            product = fields[3].split("_")[0]
            
            line = [cveid, cvss_score, provider, product, date_pub, date_mod, description]
            
            #print(cveid, provider, product)
            writer.writerow(line)
        except:
            print cveid
   
        
    csvfile.close()


if __name__ == '__main__':
    main()
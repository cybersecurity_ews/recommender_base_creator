'''
Created on Apr 23, 2016

@author: rodrigo
'''
import json
import sqlite3


class CveDatabase(object):
    def __init__(self):
        try:
            self.db = 'vfeed.db'
            self.conn = sqlite3.connect(self.db)
            self.cur = self.conn.cursor()
        except Exception, e:
            print '[!] something occurred while opening the database', e

    #cveid,date_published, date_modified,summary text, cvss_base,cvss_impact, cvss_exploit,
    #cvss_access_vector,cvss_access_complexity,cvss_authentication,cvss_confidentiality_impact
    #cvss_integrity_impact, cvss_availability_impact
    def search_cve(self, query):
        self.cur.execute("SELECT * from nvd_db where cveid like ?", ('%' + query + '%',))
        resultset = self.cur.fetchall()
        return resultset
    
    # cpeid (format sample: cpe:/o:microsoft:windows_8.1:-)
    def search_cpe(self, query_cve):
        self.cur.execute('SELECT cpeid FROM cve_cpe WHERE cveid=?', (query_cve,))
        resultset = self.cur.fetchall()
        return resultset

    
